FROM bioconductor/bioconductor_docker:3.17

RUN R -e "install.packages('remotes')"
RUN R -e "install.packages('getopt')"

# minfi mwsill fork for EPICv2 compatibility
RUN R -e "BiocManager::install('IlluminaHumanMethylation450kmanifest')"
RUN R -e "BiocManager::install('IlluminaHumanMethylationEPICmanifest')"
RUN R -e "remotes::install_github('mwsill/IlluminaHumanMethylationEPICv2manifest')"
RUN R -e "remotes::install_github('mwsill/minfi')"

# SeSAMe setup (includes fix for openblas multithreading issue https://support.bioconductor.org/p/122925/)
RUN R -e "BiocManager::install('sesame')"
RUN R -e "BiocManager::install('preprocessCore', configure.args='--disable-threading', type='source', force = TRUE, rebuild = TRUE)"

# download SeSAMe data to fixed location
RUN mkdir /sesameData && echo "options(EXPERIMENT_HUB_CACHE='/sesameData')" >> "$R_HOME"/etc/Rprofile.site
RUN R -e "library(sesame) ; sesameDataCache()"
